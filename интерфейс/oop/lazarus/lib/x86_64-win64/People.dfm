object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 1000
  ClientWidth = 960
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []

  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 368
    Top = 497
    Width = 24
    Height = 13
    Caption = 'Интуиция'
  end
  object Label2: TLabel
    Left = 368
    Top = 528
    Width = 21
    Height = 13
    Caption = 'Логика'
  end
  object Label3: TLabel
    Left = 172
    Top = 501
    Width = 17
    Height = 13
    Caption = 'Этика'
  end
  object Label4: TLabel
    Left = 172
    Top = 528
    Width = 22
    Height = 13
    Caption = 'Сенсорика'
  end
  object Label5: TLabel
    Left = 32
    Top = 479
    Width = 141
    Height = 13
    Caption = 'Информация о человеке (В баллах)'
  end
  object ChengeAspBt: TButton
    Left = 510
    Top = 500
    Width = 163
    Height = 25
    Caption = 'make aspect'
    TabOrder = 0
    OnClick = ChengeAspBtClick
  end
  object IntuitionEd: TEdit
    Left = 232
    Top = 494
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '1'
    TextHint = 'Интуиция'
  end
  object Button2: TButton
    Left = 510
    Top = 531
    Width = 163
    Height = 25
    Caption = 'information from the outside'
    TabOrder = 2
    OnClick = InfoRecBtClick
  end
  object LogicEd: TEdit
    Left = 232
    Top = 525
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '1'
    TextHint = 'Логика'
  end
  object EthicsEd: TEdit
    Left = 32
    Top = 498
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '1'
    TextHint = 'Этика'
  end
  object SensEd: TEdit
    Left = 32
    Top = 525
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '1'
    TextHint = 'Сенсорика'
  end
  object isExtrRb: TRadioButton
    Left = 712
    Top = 535
    Width = 113
    Height = 17
    Caption = 'Экстраверт'
    TabOrder = 6
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 900
    Height = 265
    ColCount = 9
    DefaultColWidth =129
    RowCount = 10
    TabOrder = 7
    ColWidths = (
      129
      50
      129
      50
      129
      50
      129
      50
      129)
    RowHeights = (
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24)
  end
end
